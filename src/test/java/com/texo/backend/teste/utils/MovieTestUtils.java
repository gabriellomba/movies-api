package com.texo.backend.teste.utils;

import java.util.Random;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import com.texo.backend.teste.dto.MovieDto;
import com.texo.backend.teste.model.Movie;
import com.texo.backend.teste.model.Producer;
import com.texo.backend.teste.model.Studio;

public class MovieTestUtils {

	private static final int RANDOM_MOVIES_QUANTITY = 20;

	public ProducerTestUtils producerTestUtils = new ProducerTestUtils();

	public StudioTestUtils studioTestUtils = new StudioTestUtils();

	public Movie createMovie(Integer year, String title, Boolean winner) {
		return createMovie(year, title, winner, producerTestUtils.randomProducer(), studioTestUtils.randomStudio());
	}

	public Movie createMovieWithProducer(Integer year, String title, Boolean winner, Producer producer) {
		return createMovie(year, title, winner, producer, studioTestUtils.randomStudio());
	}

	public Movie createMovieWithStudio(Integer year, String title, Boolean winner, Studio studio) {
		return createMovie(year, title, winner, producerTestUtils.randomProducer(), studio);
	}
	
	public MovieDto randomMovieDto() {
		final Random rand = new Random();
		final Integer id = rand.nextInt();
		final Integer year = rand.nextInt();
		final String title = randomMovieName();
		final Boolean winner = rand.nextInt() % 2 == 0;
		return new MovieDto(id, year, title, studioTestUtils.randomStudioNames(),
				producerTestUtils.randomProducerNames(), winner);
	}

	public Set<MovieDto> randomMovieDtos() {
		return IntStream.of(RANDOM_MOVIES_QUANTITY)
				.mapToObj(i -> randomMovieDto())
				.collect(Collectors.toSet());
	}

	public Set<Movie> createRandomMovies() {
		final Random rand = new Random();

		return IntStream.of(RANDOM_MOVIES_QUANTITY).mapToObj(i -> {
			int randInt = rand.nextInt();
			return createMovie(randInt, String.format("Filme %d", randInt), randInt % 2 == 0);
		}).collect(Collectors.toSet());
	}

	private Movie createMovie(Integer year, String title, Boolean winner, Producer producer, Studio studio) {
		final Movie movie = new Movie(year, title, winner);
		movie.addProducer(producer);
		movie.addStudio(studio);

		return movie;
	}
	
	private String randomMovieName() {
		return "M" + new Random().nextInt();
	}
}
