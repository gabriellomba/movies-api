package com.texo.backend.teste.utils;

import java.util.Collection;

import org.assertj.core.util.Arrays;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;

public class DbUtils {

	private final TestEntityManager entityManager;

	public DbUtils(TestEntityManager entityManager) {
		this.entityManager = entityManager;
	}

	public void insertToDb(Object... objects) {
		insertToDb(Arrays.asList(objects));
	}

	public void insertToDb(Collection<?> objects) {
		for (Object object : objects) {
			entityManager.persist(object);
		}
		entityManager.flush();
	}
}
