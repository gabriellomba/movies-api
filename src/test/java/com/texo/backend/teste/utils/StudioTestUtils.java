package com.texo.backend.teste.utils;

import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import com.texo.backend.teste.dto.StudioWinCountDto;
import com.texo.backend.teste.model.Studio;

public class StudioTestUtils {

	private static final int RANDOM_STUDIOS_QUANTITY = 20;

	public Studio randomStudio() {
		return new Studio(randomStudioName());
	}

	public StudioWinCountDto randomWinCountDto() {
		final Random rand = new Random();
		final Long winnerCount = rand.nextLong();
		return new StudioWinCountDto(randomStudioName(), winnerCount);
	}

	public List<StudioWinCountDto> randomWinCountDtos() {
		return IntStream.of(RANDOM_STUDIOS_QUANTITY)
				.mapToObj(i -> randomWinCountDto())
				.collect(Collectors.toList());
	}

	private String randomStudioName() {
		return "S" + new Random().nextInt();
	}

	public Set<String> randomStudioNames() {
		return IntStream.of(RANDOM_STUDIOS_QUANTITY)
				.mapToObj(i -> randomStudioName())
				.collect(Collectors.toSet());
	}
}
