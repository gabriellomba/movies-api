package com.texo.backend.teste.utils;

import java.util.Random;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import com.texo.backend.teste.dto.ProducerWinIntervalDto;
import com.texo.backend.teste.model.Producer;

public class ProducerTestUtils {

	private static final int RANDOM_PRODUCERS_QUANTITY = 20;
	
	public Producer randomProducer() {
		return new Producer(randomProducerName());
	}
	
	public ProducerWinIntervalDto randomWinIntervalDto() {
		final Random rand = new Random();
		final Integer previousWin = rand.nextInt();
		final Integer followingWin = rand.nextInt();
		return new ProducerWinIntervalDto(randomProducerName(), followingWin - previousWin, previousWin, followingWin);
	}
	
	private String randomProducerName() {
		return "P" + new Random().nextInt();
	}
	
	public Set<String> randomProducerNames() {
		return IntStream.of(RANDOM_PRODUCERS_QUANTITY)
				.mapToObj(i -> randomProducerName())
				.collect(Collectors.toSet());
	}
}
