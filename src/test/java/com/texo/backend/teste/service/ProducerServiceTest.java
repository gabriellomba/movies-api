package com.texo.backend.teste.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

import java.util.Collections;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.junit4.SpringRunner;

import com.texo.backend.teste.CustomTestConfiguration;
import com.texo.backend.teste.dao.ProducerDao;
import com.texo.backend.teste.dto.ProducerMinMaxWinIntervalResponseDto;
import com.texo.backend.teste.dto.ProducerWinIntervalDto;
import com.texo.backend.teste.error.ProducerWithMoreThanOneWinNotFoundException;
import com.texo.backend.teste.utils.ProducerTestUtils;

@RunWith(SpringRunner.class)
@SpringBootTest
@Import(CustomTestConfiguration.class)
public class ProducerServiceTest {

	@Autowired
	private ProducerService producerService;

	@MockBean
	private ProducerDao mockProducerDao;

	public ProducerTestUtils producerTestUtils = new ProducerTestUtils();

	@Test(expected = ProducerWithMoreThanOneWinNotFoundException.class)
	public void givenNoProducersWithTwoOrMoreWinsWhenGetProducersMinMaxWinIntervalThenThrowException() {
		final Pageable page = PageRequest.of(0, 1);
		when(mockProducerDao.getProducerMinWinInterval(page)).thenReturn(Collections.emptyList());
		when(mockProducerDao.getProducerMaxWinInterval(page)).thenReturn(Collections.emptyList());

		producerService.getProducersMinMaxWinInterval();
	}

	@Test
	public void givenWinnersInYearWhenGetWinnersThenReturnWinnersDtos() {
		final Pageable page = PageRequest.of(0, 1);
		final ProducerWinIntervalDto minWinIntervalDto = producerTestUtils.randomWinIntervalDto();
		final ProducerWinIntervalDto maxWinIntervalDto = producerTestUtils.randomWinIntervalDto();
		when(mockProducerDao.getProducerMinWinInterval(page)).thenReturn(Collections.singletonList(minWinIntervalDto));
		when(mockProducerDao.getProducerMaxWinInterval(page)).thenReturn(Collections.singletonList(maxWinIntervalDto));

		ProducerMinMaxWinIntervalResponseDto responseDto =
				producerService.getProducersMinMaxWinInterval();

		assertThat(responseDto.getMin()).isEqualTo(minWinIntervalDto);
		assertThat(responseDto.getMax()).isEqualTo(maxWinIntervalDto);
	}

}
