package com.texo.backend.teste.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.junit4.SpringRunner;

import com.texo.backend.teste.CustomTestConfiguration;
import com.texo.backend.teste.dao.MovieDao;
import com.texo.backend.teste.dto.MovieDto;
import com.texo.backend.teste.dto.YearWinCountDto;
import com.texo.backend.teste.dto.YearWinCountResponseDto;
import com.texo.backend.teste.error.MovieNotFoundException;
import com.texo.backend.teste.error.ValidationException;
import com.texo.backend.teste.model.Movie;
import com.texo.backend.teste.utils.MovieTestUtils;

@RunWith(SpringRunner.class)
@SpringBootTest
@Import(CustomTestConfiguration.class)
public class MovieServiceTest {

	@Autowired
	private MovieService movieService;

	@MockBean
	private MovieDao mockMovieDao;

	private MovieTestUtils movieTestUtils = new MovieTestUtils();

	@Test
	public void givenNoWinnersInYearWhenGetWinnersThenReturnEmptySet() {
		final int year = 2018;
		when(mockMovieDao.getWinnersInYear(year)).thenReturn(Collections.emptySet());

		final Set<MovieDto> actualValue = movieService.getWinners(year);

		assertThat(actualValue).isEmpty();
	}

	@Test
	public void givenWinnersInYearWhenGetWinnersThenReturnWinnersDtos() {
		final int year = 123;
		final Set<Movie> expectedMovies = movieTestUtils.createRandomMovies();
		final Set<String> expectedMoviesTitles = expectedMovies.stream().map(Movie::getTitle)
				.collect(Collectors.toSet());
		when(mockMovieDao.getWinnersInYear(year)).thenReturn(expectedMovies);

		final Set<MovieDto> actualValue = movieService.getWinners(year);
		final Set<String> actualMoviesTitles = actualValue.stream().map(MovieDto::getTitle).collect(Collectors.toSet());

		assertThat(actualMoviesTitles).isEqualTo(expectedMoviesTitles);
	}

	@Test(expected = MovieNotFoundException.class)
	public void givenNoMoviesWithIdWhenDeleteThenThrowMovieNotFoundException() {
		final int id = 123;
		when(mockMovieDao.findById(id)).thenReturn(Optional.empty());

		movieService.delete(id);
	}

	@Test(expected = ValidationException.class)
	public void givenMovieWithIdIsWinnerWhenDeleteThenThrowValidationException() {
		final Movie winnerMovie = movieTestUtils.createMovie(1234, "Title", true);
		final int id = 123;
		when(mockMovieDao.findById(id)).thenReturn(Optional.of(winnerMovie));

		movieService.delete(id);
	}

	@Test
	public void givenMovieWithIdIsNotWinnerWhenDeleteThenProceedWithDeletion() {
		final Movie movie = movieTestUtils.createMovie(1234, "Title", false);
		final int id = 123;
		when(mockMovieDao.findById(id)).thenReturn(Optional.of(movie));

		movieService.delete(id);

		verify(mockMovieDao, times(1)).delete(movie);
	}

	@Test
	public void givenNoYearWinCountsWhenGetYearWinCountThenReturnEmptyResponse() {
		final long biggerThan = 1234;
		when(mockMovieDao.getYearWinCount(biggerThan)).thenReturn(Collections.emptyList());

		YearWinCountResponseDto responseDto = movieService.getYearWinCount(biggerThan);

		assertThat(responseDto.getYears()).isEmpty();
	}

	@Test
	public void givenYearWinCountsWhenGetYearWinCountThenReturnResponseWithAppropriateCounts() {
		final long biggerThan = 1234;
		final List<YearWinCountDto> yearWinCounts = Arrays.asList(new YearWinCountDto(1324, 40L),
				new YearWinCountDto(1456, 3L), new YearWinCountDto(2005, 23L));
		when(mockMovieDao.getYearWinCount(biggerThan)).thenReturn(yearWinCounts);

		YearWinCountResponseDto responseDto = movieService.getYearWinCount(biggerThan);

		assertThat(responseDto.getYears()).isEqualTo(yearWinCounts);
	}
}
