package com.texo.backend.teste.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

import java.util.Collections;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.junit4.SpringRunner;

import com.texo.backend.teste.CustomTestConfiguration;
import com.texo.backend.teste.dao.StudioDao;
import com.texo.backend.teste.dto.StudioWinCountDto;
import com.texo.backend.teste.dto.StudioWinCountResponseDto;
import com.texo.backend.teste.utils.StudioTestUtils;

@RunWith(SpringRunner.class)
@SpringBootTest
@Import(CustomTestConfiguration.class)
public class StudioServiceTest {

	@Autowired
	private StudioService studioService;

	@MockBean
	private StudioDao mockStudioDao;

	public StudioTestUtils studioTestUtils = new StudioTestUtils();

	@Test
	public void givenNoWinnerStudiosWhenGetStudiosWinCountThenReturnEmptyArray() {
		when(mockStudioDao.getStudiosWinCount()).thenReturn(Collections.emptyList());

		StudioWinCountResponseDto responseDto = studioService.getStudiosWinCount();

		assertThat(responseDto.getStudios()).isEmpty();
	}

	@Test
	public void givenWinnerStudiosWhenGetStudiosWinCountThenReturnArrayOfWinners() {
		List<StudioWinCountDto> expectedDtos = studioTestUtils.randomWinCountDtos();
		when(mockStudioDao.getStudiosWinCount()).thenReturn(expectedDtos);

		StudioWinCountResponseDto responseDto = studioService.getStudiosWinCount();

		assertThat(responseDto.getStudios()).isEqualTo(expectedDtos);
	}

}
