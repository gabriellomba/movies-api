package com.texo.backend.teste.controller;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.Set;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.junit4.SpringRunner;

import com.texo.backend.teste.CustomTestConfiguration;
import com.texo.backend.teste.dto.MovieDto;
import com.texo.backend.teste.dto.YearWinCountDto;
import com.texo.backend.teste.dto.YearWinCountResponseDto;
import com.texo.backend.teste.service.MovieService;
import com.texo.backend.teste.utils.MovieTestUtils;

@RunWith(SpringRunner.class)
@SpringBootTest
@Import(CustomTestConfiguration.class)
public class MovieControllerTest {

	@Autowired
	private MovieController movieController;

	@MockBean
	private MovieService mockMovieService;

	private MovieTestUtils movieTestUtils = new MovieTestUtils();

	@Test
	public void givenNoWinnerMoviesInYearWhenGetWinnersThenReturnArrayOfWinners() {
		final int year = 132456;
		when(mockMovieService.getWinners(year)).thenReturn(Collections.emptySet());

		Set<MovieDto> actualDtos = movieController.getWinners(year);

		assertThat(actualDtos).isEmpty();
	}
	
	@Test
	public void givenWinnerMoviesInYearWhenGetWinnersThenReturnArrayOfWinners() {
		final int year = 132456;
		Set<MovieDto> expectedDtos = movieTestUtils.randomMovieDtos();
		when(mockMovieService.getWinners(year)).thenReturn(expectedDtos);

		Set<MovieDto> actualDtos = movieController.getWinners(year);

		assertThat(actualDtos).isEqualTo(expectedDtos);
	}
	
	@Test
	public void givenNoYearWinCountsBiggerWhenGetYearWinCountThenReturnEmptyArray() {
		final long biggerThan = 10L;
		when(mockMovieService.getYearWinCount(biggerThan)).thenReturn(
				new YearWinCountResponseDto(Collections.emptyList()));

		YearWinCountResponseDto response = movieController.getYearWinCount(biggerThan);

		assertThat(response.getYears()).isEmpty();
	}
	
	@Test
	public void givenYearWinCountsBiggerWhenGetYearWinCountThenReturnArrayOfWinCounts() {
		final long biggerThan = 20L;
		final Random rand = new Random();
		final List<YearWinCountDto> expectedDtos = Arrays.asList(
				new YearWinCountDto(rand.nextInt(), rand.nextLong()), new YearWinCountDto(rand.nextInt(), rand.nextLong()),
				new YearWinCountDto(rand.nextInt(), rand.nextLong()), new YearWinCountDto(rand.nextInt(), rand.nextLong()));
		when(mockMovieService.getYearWinCount(biggerThan)).thenReturn(new YearWinCountResponseDto(expectedDtos));

		YearWinCountResponseDto response = movieController.getYearWinCount(biggerThan);

		assertThat(response.getYears()).isEqualTo(expectedDtos);
	}
	
	@Test
	public void givenRequestForMovieDeletionWhenDeleteMovieThenForwardToService() {
		final int movieId = 1231564;

		movieController.deleteMovie(movieId);

		verify(mockMovieService, times(1)).delete(movieId);
	}
}
