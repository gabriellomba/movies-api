package com.texo.backend.teste.controller;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.junit4.SpringRunner;

import com.texo.backend.teste.CustomTestConfiguration;
import com.texo.backend.teste.dto.StudioWinCountDto;
import com.texo.backend.teste.dto.StudioWinCountResponseDto;
import com.texo.backend.teste.service.StudioService;
import com.texo.backend.teste.utils.StudioTestUtils;

@RunWith(SpringRunner.class)
@SpringBootTest
@Import(CustomTestConfiguration.class)
public class StudioControllerTest {

	@Autowired
	private StudioController studioController;

	@MockBean
	private StudioService mockStudioService;

	private StudioTestUtils studioTestUtils = new StudioTestUtils();

	@Test
	public void givenWinnerMoviesInYearWhenGetWinnersThenReturnArrayOfWinners() {
		List<StudioWinCountDto> winCountDtos = studioTestUtils.randomWinCountDtos();
		StudioWinCountResponseDto expectedDto = new StudioWinCountResponseDto(winCountDtos);
		when(mockStudioService.getStudiosWinCount()).thenReturn(expectedDto);

		StudioWinCountResponseDto actualDto = studioController.getStudiosWinCount();

		assertThat(actualDto).isEqualTo(expectedDto);
	}
}
