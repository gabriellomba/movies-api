package com.texo.backend.teste.controller;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.junit4.SpringRunner;

import com.texo.backend.teste.CustomTestConfiguration;
import com.texo.backend.teste.dto.ProducerMinMaxWinIntervalResponseDto;
import com.texo.backend.teste.dto.ProducerWinIntervalDto;
import com.texo.backend.teste.service.ProducerService;
import com.texo.backend.teste.utils.ProducerTestUtils;

@RunWith(SpringRunner.class)
@SpringBootTest
@Import(CustomTestConfiguration.class)
public class ProducerControllerTest {

	@Autowired
	private ProducerController producerController;

	@MockBean
	private ProducerService mockProducerService;

	private ProducerTestUtils producerTestUtils = new ProducerTestUtils();
	
	@Test
	public void givenMinMaxWinIntervalsWhenGetProducerMinMaxWinIntervalThenReturnCorrectIntervals() {
		ProducerWinIntervalDto minWinIntervalDto = producerTestUtils.randomWinIntervalDto();
		ProducerWinIntervalDto maxWinIntervalDto = producerTestUtils.randomWinIntervalDto();
		ProducerMinMaxWinIntervalResponseDto expectedDto = new ProducerMinMaxWinIntervalResponseDto(minWinIntervalDto, maxWinIntervalDto);
		when(mockProducerService.getProducersMinMaxWinInterval()).thenReturn(expectedDto);

		ProducerMinMaxWinIntervalResponseDto actualDto = producerController.getProducerMinMaxWinInterval();

		assertThat(actualDto).isEqualTo(expectedDto);
	}
}
