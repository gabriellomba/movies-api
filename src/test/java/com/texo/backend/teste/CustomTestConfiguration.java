package com.texo.backend.teste;

import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;

import com.texo.backend.teste.listener.ApplicationReadyListener;

@TestConfiguration
public class CustomTestConfiguration {

	@MockBean
	private ApplicationReadyListener applicationReadyListener;
	
	@Bean
	@Primary
	public ApplicationReadyListener applicationReadyListener() {
		return applicationReadyListener;
	}

}
