package com.texo.backend.teste.csv;

import static org.assertj.core.api.Assertions.assertThat;

import java.io.IOException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.junit4.SpringRunner;

import com.texo.backend.teste.CustomTestConfiguration;

@RunWith(SpringRunner.class)
@SpringBootTest(properties = { "movies-file.path=src/test/resources/csv/movielist35Entries.csv" })
@Import(CustomTestConfiguration.class)
public class MovieCsvReaderTest {

	private static final int EXPECTED_SIZE = 35;

	@Autowired
	private MovieCsvReader movieCsvReader;

	@Test
	public void given35EntriesInCsvWhenGetMoviesThenReturnMovieEntries() throws IOException {
		assertThat(movieCsvReader.getMovies()).hasSize(EXPECTED_SIZE);
	}
}
