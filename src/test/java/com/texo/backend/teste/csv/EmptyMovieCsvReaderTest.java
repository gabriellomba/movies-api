package com.texo.backend.teste.csv;

import java.io.IOException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.junit4.SpringRunner;

import com.texo.backend.teste.CustomTestConfiguration;
import com.texo.backend.teste.error.EmptyCsvException;

@RunWith(SpringRunner.class)
@SpringBootTest(properties = { "movies-file.path=src/test/resources/csv/emptyMovielist.csv" })
@Import(CustomTestConfiguration.class)
public class EmptyMovieCsvReaderTest {

	@Autowired
	private MovieCsvReader movieCsvReader;

	@Test(expected = EmptyCsvException.class)
	public void givenNoDataInCsvWhenGetMoviesThenThrowEmptyCsvException() throws IOException {
		movieCsvReader.getMovies();
	}
}
