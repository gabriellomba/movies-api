package com.texo.backend.teste.dao;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.junit4.SpringRunner;

import com.texo.backend.teste.CustomTestConfiguration;
import com.texo.backend.teste.dto.StudioWinCountDto;
import com.texo.backend.teste.model.Studio;
import com.texo.backend.teste.utils.DbUtils;
import com.texo.backend.teste.utils.MovieTestUtils;

@RunWith(SpringRunner.class)
@DataJpaTest
@Import(CustomTestConfiguration.class)
public class StudioDaoTest {

	@Autowired
	private StudioDao studioDao;

	@Autowired
	private TestEntityManager entityManager;

	private DbUtils dbUtils;

	private MovieTestUtils movieTestUtils = new MovieTestUtils();

	@Before
	public void initUtils() {
		entityManager.clear();
		dbUtils = new DbUtils(entityManager);
	}

	@Test
	public void givenNoWinnerStudiosWhenGetStudiosWinCountThenReturnEmptyArray() {
		final Studio s1 = new Studio("S1");
		final Studio s2 = new Studio("S2");
		dbUtils.insertToDb(movieTestUtils.createMovieWithStudio(1980, "Test 1", false, s1),
				movieTestUtils.createMovieWithStudio(1980, "Test 2", false, s2));

		final List<StudioWinCountDto> studiosWinCount = studioDao.getStudiosWinCount();

		assertThat(studiosWinCount.isEmpty()).isTrue();
	}

	@Test
	public void givenWinnerStudiosWhenGetStudiosWinCountThenReturnArrayOfWinners() {
		final Studio s1 = new Studio("S1");
		final Studio s2 = new Studio("S2");
		final Studio s3 = new Studio("S3");
		dbUtils.insertToDb(movieTestUtils.createMovieWithStudio(1980, "Test 1", true, s1),
				movieTestUtils.createMovieWithStudio(1990, "Test 2", true, s1),
				movieTestUtils.createMovieWithStudio(1950, "Test 3", false, s1),
				movieTestUtils.createMovieWithStudio(1925, "Test 4", true, s2),
				movieTestUtils.createMovieWithStudio(1946, "Test 5", false, s2),
				movieTestUtils.createMovieWithStudio(2010, "Test 6", false, s3));

		final List<StudioWinCountDto> studiosWinCount = studioDao.getStudiosWinCount();

		assertThat(studiosWinCount.size()).isEqualTo(2);
		assertThat(studiosWinCount.stream().filter(swc -> swc.getName().equals(s1.getName()) && swc.getWinCount() == 2)
				.findAny()).isPresent();
		assertThat(studiosWinCount.stream().filter(swc -> swc.getName().equals(s2.getName()) && swc.getWinCount() == 1)
				.findAny()).isPresent();
		assertThat(studiosWinCount.stream().filter(swc -> swc.getName().equals(s3.getName())).findAny()).isNotPresent();
	}
}
