package com.texo.backend.teste.dao;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.context.annotation.Import;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.junit4.SpringRunner;

import com.texo.backend.teste.CustomTestConfiguration;
import com.texo.backend.teste.dto.ProducerWinIntervalDto;
import com.texo.backend.teste.model.Producer;
import com.texo.backend.teste.utils.DbUtils;
import com.texo.backend.teste.utils.MovieTestUtils;

@RunWith(SpringRunner.class)
@DataJpaTest
@Import(CustomTestConfiguration.class)
public class ProducerDaoTest {

	@Autowired
	private ProducerDao producerDao;

	@Autowired
	private TestEntityManager entityManager;

	private DbUtils dbUtils;
	
	private MovieTestUtils movieTestUtils = new MovieTestUtils();

	@Before
	public void initUtils() {
		entityManager.clear();
		dbUtils = new DbUtils(entityManager);
	}

	@Test
	public void givenNoProducersWithTwoOrMoreWinsWhenGetProducerMinWinIntervalThenReturnEmptyArray() {
		final Producer p1 = new Producer("P1");
		final Producer p2 = new Producer("P2");
		dbUtils.insertToDb(movieTestUtils.createMovieWithProducer(1980, "Test 1", true, p1),
				movieTestUtils.createMovieWithProducer(1980, "Test 2", true, p2));

		final List<ProducerWinIntervalDto> producersWinIntervals = producerDao.getProducerMinWinInterval(PageRequest.of(0, 1));

		assertThat(producersWinIntervals.isEmpty()).isTrue();
	}
	
	@Test
	public void givenProducersWithTwoOrMoreWinsWhenGetProducerMinWinIntervalThenReturnProducerWithMinWinInterval() {
		final Producer p1 = new Producer("P1");
		final Producer p2 = new Producer("P2");
		dbUtils.insertToDb(movieTestUtils.createMovieWithProducer(1980, "Test 1", true, p1),
				movieTestUtils.createMovieWithProducer(1985, "Test 2", true, p1),
				movieTestUtils.createMovieWithProducer(1987, "Test 3", true, p2),
				movieTestUtils.createMovieWithProducer(1989, "Test 4", true, p2));

		final List<ProducerWinIntervalDto> producersWinIntervals = producerDao.getProducerMinWinInterval(PageRequest.of(0, 1));

		assertThat(producersWinIntervals.size()).isOne();
		assertThat(producersWinIntervals.get(0).getProducer()).isEqualTo(p2.getName());
		assertThat(producersWinIntervals.get(0).getInterval()).isEqualTo(2);
	}
	
	@Test
	public void givenNoProducersWithTwoOrMoreWinsWhenGetProducerMaxWinIntervalThenReturnEmptyArray() {
		final Producer p1 = new Producer("P1");
		final Producer p2 = new Producer("P2");
		dbUtils.insertToDb(movieTestUtils.createMovieWithProducer(1980, "Test 1", true, p1),
				movieTestUtils.createMovieWithProducer(1980, "Test 2", true, p2));

		final List<ProducerWinIntervalDto> producersWinIntervals = producerDao.getProducerMaxWinInterval(PageRequest.of(0, 1));

		assertThat(producersWinIntervals.isEmpty()).isTrue();
	}
	
	@Test
	public void givenProducersWithTwoOrMoreWinsWhenGetProducerMaxWinIntervalThenReturnProducerWithMinWinInterval() {
		final Producer p1 = new Producer("P1");
		final Producer p2 = new Producer("P2");
		dbUtils.insertToDb(movieTestUtils.createMovieWithProducer(1980, "Test 1", true, p1),
				movieTestUtils.createMovieWithProducer(1985, "Test 2", true, p1),
				movieTestUtils.createMovieWithProducer(1987, "Test 3", true, p2),
				movieTestUtils.createMovieWithProducer(1989, "Test 4", true, p2));

		final List<ProducerWinIntervalDto> producersWinIntervals = producerDao.getProducerMaxWinInterval(PageRequest.of(0, 1));

		assertThat(producersWinIntervals.size()).isOne();
		assertThat(producersWinIntervals.get(0).getProducer()).isEqualTo(p1.getName());
		assertThat(producersWinIntervals.get(0).getInterval()).isEqualTo(5);
	}
	
	@Test
	public void givenProducersWithThreeOrMoreWinsWhenGetProducerMaxWinIntervalThenAccountOnlyMaxConsecutiveWins() {
		final Producer p1 = new Producer("P1");
		final Producer p2 = new Producer("P2");
		dbUtils.insertToDb(movieTestUtils.createMovieWithProducer(1980, "Test 1", true, p1),
				movieTestUtils.createMovieWithProducer(1988, "Test 2", true, p1),
				movieTestUtils.createMovieWithProducer(1999, "Test 3", true, p1),
				movieTestUtils.createMovieWithProducer(1987, "Test 4", true, p2),
				movieTestUtils.createMovieWithProducer(2000, "Test 5", true, p2),
				movieTestUtils.createMovieWithProducer(2011, "Test 6", true, p2));

		final List<ProducerWinIntervalDto> producersWinIntervals = producerDao.getProducerMaxWinInterval(PageRequest.of(0, 1));

		assertThat(producersWinIntervals.size()).isOne();
		assertThat(producersWinIntervals.get(0).getProducer()).isEqualTo(p2.getName());
		assertThat(producersWinIntervals.get(0).getInterval()).isEqualTo(13);
	}
}
