package com.texo.backend.teste.dao;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.junit4.SpringRunner;

import com.texo.backend.teste.CustomTestConfiguration;
import com.texo.backend.teste.dto.YearWinCountDto;
import com.texo.backend.teste.model.Movie;
import com.texo.backend.teste.utils.DbUtils;
import com.texo.backend.teste.utils.MovieTestUtils;

@RunWith(SpringRunner.class)
@DataJpaTest
@Import(CustomTestConfiguration.class)
public class MovieDaoTest {

	@Autowired
	private MovieDao movieDao;

	@Autowired
	private TestEntityManager entityManager;

	private DbUtils dbUtils;

	private MovieTestUtils movieTestUtils = new MovieTestUtils();

	@Before
	public void initUtils() {
		entityManager.clear();
		dbUtils = new DbUtils(entityManager);
	}

	@Test
	public void givenNoWinnersInYearWhenGetWinnersOfYearThenReturnEmptyArray() {
		dbUtils.insertToDb(movieTestUtils.createMovie(1980, "Test 1", false),
				movieTestUtils.createMovie(1985, "Test 2", true));

		final Set<Movie> movies = movieDao.getWinnersInYear(1980);

		assertThat(movies.isEmpty()).isTrue();
	}

	@Test
	public void givenWinnersInYearWhenGetWinnersOfYearThenReturnArrayOfWinners() {
		final Movie m1 = movieTestUtils.createMovie(1985, "Test 2", true);
		final Movie m2 = movieTestUtils.createMovie(1985, "Test 3", true);
		dbUtils.insertToDb(m1, m2, movieTestUtils.createMovie(1980, "Test 1", false),
				movieTestUtils.createMovie(1985, "Test 4", false));

		final Set<Movie> movies = movieDao.getWinnersInYear(1985);

		assertThat(movies.size()).isEqualTo(2);
		assertThat(movies).contains(m1, m2);
	}

	@Test
	public void givenNoWinnersWhenGetYearWinCountThenReturnEmptyArray() {
		final List<YearWinCountDto> yearWinCount = movieDao.getYearWinCount(0L);

		assertThat(yearWinCount.isEmpty()).isTrue();
	}

	@Test
	public void givenNoYearsWithWinnersMoreThanBiggerThanWhenGetYearWinCountThenReturnEmptyArray() {
		dbUtils.insertToDb(movieTestUtils.createMovie(1980, "Test 1", true),
				movieTestUtils.createMovie(1985, "Test 2", true), movieTestUtils.createMovie(1981, "Test 3", true),
				movieTestUtils.createMovie(1984, "Test 4", true));

		final List<YearWinCountDto> yearWinCount = movieDao.getYearWinCount(1L);

		assertThat(yearWinCount.isEmpty()).isTrue();
	}

	@Test
	public void givenYearsWithMoreWinnersThanBiggerThanWhenGetYearWinCountThenReturnArrayOfYearWinCounts() {
		dbUtils.insertToDb(movieTestUtils.createMovie(1981, "Test 1", true),
				movieTestUtils.createMovie(1985, "Test 2", true), movieTestUtils.createMovie(1981, "Test 3", true),
				movieTestUtils.createMovie(1984, "Test 4", true));

		final List<YearWinCountDto> yearWinCount = movieDao.getYearWinCount(1L);

		assertThat(yearWinCount.size()).isOne();
		assertThat(yearWinCount.get(0).getYear()).isEqualTo(1981);
	}
}
