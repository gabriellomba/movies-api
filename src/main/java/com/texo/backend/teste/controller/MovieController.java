package com.texo.backend.teste.controller;

import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.texo.backend.teste.dto.MovieDto;
import com.texo.backend.teste.dto.YearWinCountResponseDto;
import com.texo.backend.teste.service.MovieService;

@RestController
@RequestMapping(value = "/movies")
public class MovieController {

	@Autowired
	private MovieService movieService;

	@RequestMapping(value = "/winners", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public Set<MovieDto> getWinners(@RequestParam("year") Integer year) {
		return movieService.getWinners(year);
	}

	@RequestMapping(value = "/yearWinCount", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public YearWinCountResponseDto getYearWinCount(@RequestParam("biggerThan") Long biggerThan) {
		return movieService.getYearWinCount(biggerThan);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public void deleteMovie(@PathVariable("id") Integer id) {
		movieService.delete(id);
	}
}
