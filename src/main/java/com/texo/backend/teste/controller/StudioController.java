package com.texo.backend.teste.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.texo.backend.teste.dto.StudioWinCountResponseDto;
import com.texo.backend.teste.service.StudioService;

@RestController
@RequestMapping(value = "/studios")
public class StudioController {

	@Autowired
	private StudioService studioService;

	@RequestMapping(value = "/winCount", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public StudioWinCountResponseDto getStudiosWinCount() {
		return studioService.getStudiosWinCount();
	}
}
