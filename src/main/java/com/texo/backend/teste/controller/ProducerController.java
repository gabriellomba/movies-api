package com.texo.backend.teste.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.texo.backend.teste.dto.ProducerMinMaxWinIntervalResponseDto;
import com.texo.backend.teste.service.ProducerService;

@RestController
@RequestMapping(value = "/producers")
public class ProducerController {

	@Autowired
	private ProducerService producerService;

	@RequestMapping(value = "/minMaxWinInterval", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ProducerMinMaxWinIntervalResponseDto getProducerMinMaxWinInterval() {
		return producerService.getProducersMinMaxWinInterval();
	}
}
