package com.texo.backend.teste.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.texo.backend.teste.dto.StudioWinCountDto;
import com.texo.backend.teste.model.Movie;

@Repository
public interface StudioDao extends JpaRepository<Movie, Integer> {

	@Query(
		"SELECT new com.texo.backend.teste.dto.StudioWinCountDto(s.name, COUNT(s)) " +
		"FROM Studio s " +
		"JOIN s.movies m " +
		"WHERE m.winner = true " +
		"GROUP BY s.id " +
		"ORDER BY COUNT(s) DESC")
	public List<StudioWinCountDto> getStudiosWinCount();
}
