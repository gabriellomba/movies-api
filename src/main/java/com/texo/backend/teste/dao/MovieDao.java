package com.texo.backend.teste.dao;

import java.util.List;
import java.util.Set;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.texo.backend.teste.dto.YearWinCountDto;
import com.texo.backend.teste.model.Movie;

@Repository
public interface MovieDao extends CrudRepository<Movie, Integer> {

	@Query("FROM Movie m JOIN FETCH m.producers JOIN FETCH m.studios WHERE m.winner = true AND m.year = ?1")
	public Set<Movie> getWinnersInYear(Integer year);

	@Query(
		"SELECT new com.texo.backend.teste.dto.YearWinCountDto(m.year, COUNT(m)) " +
		"FROM Movie m " +
		"WHERE m.winner = true " +
		"GROUP BY m.year " +
		"HAVING COUNT(m) > ?1")
	public List<YearWinCountDto> getYearWinCount(Long biggerThan);
}
