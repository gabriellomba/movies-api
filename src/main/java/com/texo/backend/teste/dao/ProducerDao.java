package com.texo.backend.teste.dao;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.texo.backend.teste.dto.ProducerWinIntervalDto;
import com.texo.backend.teste.model.Movie;

@Repository
public interface ProducerDao extends JpaRepository<Movie, Integer> {

	@Query(
		"SELECT new com.texo.backend.teste.dto.ProducerWinIntervalDto(p.name, m2.year - m1.year, m1.year, m2.year) " +
		"FROM Producer p " +
		"    JOIN p.movies m1 " +
		"    JOIN p.movies m2 " +
		"WHERE m1.winner = true " +
		"    AND m2.winner = true " + 
		"    AND m1.id != m2.id " + 
		"    AND m2.year >= m1.year " +
		"GROUP BY p.id, m1.year, m2.year " +
		"ORDER BY m2.year - m1.year")
	public List<ProducerWinIntervalDto> getProducerMinWinInterval(Pageable pageable);

	@Query(
			"SELECT new com.texo.backend.teste.dto.ProducerWinIntervalDto(p1.name, m2.year - m1.year, m1.year, m2.year) " +
			"FROM Producer p1 " +
			"    JOIN p1.movies m1 " +
			"    JOIN p1.movies m2 " +
			"WHERE m1.winner = true " +
			"    AND m2.winner = true " + 
			"    AND m1.id != m2.id " + 
			"    AND m2.year >= m1.year " +
			"    AND NOT EXISTS(" +
			"        SELECT 1 FROM Movie m3 JOIN m3.producers p2" +
			"        WHERE p2.id = p1.id AND m3.winner = true" +
			"            AND m3.year > m1.year AND m3.year < m2.year" +
			"    ) " + 
			"GROUP BY p1.id, m1.year, m2.year " +
			"ORDER BY m2.year - m1.year DESC")
	public List<ProducerWinIntervalDto> getProducerMaxWinInterval(Pageable pageable);
}
