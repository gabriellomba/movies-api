package com.texo.backend.teste.error;

import org.springframework.http.HttpStatus;

public final class MovieNotFoundException extends AbstractHttpException {

	private static final long serialVersionUID = 4083832976308045965L;
	private static final String MOVIE_NOT_FOUND_MESSAGE = "Não foi encontrado um filme com o id %d";

	public MovieNotFoundException(Integer id) {
		super(HttpStatus.NOT_FOUND, String.format(MOVIE_NOT_FOUND_MESSAGE, id));
	}
}
