package com.texo.backend.teste.error;

import org.springframework.http.HttpStatus;

public class ValidationException extends AbstractHttpException {

	private static final long serialVersionUID = -2450670914849663719L;

	public ValidationException(HttpStatus status, String message) {
		super(status, message);
	}

}
