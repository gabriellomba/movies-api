package com.texo.backend.teste.error;

public final class EmptyCsvException extends RuntimeException {

	private static final long serialVersionUID = 2099124885352143213L;
	private static final String EMPTY_CSV_MESSAGE = "Não há dados no arquivo csv passado para leitura.";

	public EmptyCsvException() {
		super(EMPTY_CSV_MESSAGE);
	}
}
