package com.texo.backend.teste.error;

import org.springframework.http.HttpStatus;

public final class ProducerWithMoreThanOneWinNotFoundException extends AbstractHttpException {

	private static final long serialVersionUID = -9027211650029773852L;
	private static final String PRODUCER_NOT_FOUND_MESSAGE =
			"Não foram encontrados produtores que tenham ganhado o prêmio mais do que 1 vez.";

	public ProducerWithMoreThanOneWinNotFoundException() {
		super(HttpStatus.NOT_FOUND, PRODUCER_NOT_FOUND_MESSAGE);
	}
}
