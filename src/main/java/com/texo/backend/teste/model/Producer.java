package com.texo.backend.teste.model;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name = "PRODUCER")
public class Producer implements Serializable {

	private static final long serialVersionUID = 1504152317659295709L;

	@Id
	@GeneratedValue
	@Column(name = "ID")
	private Integer id;

	@Column(name = "NAME", unique = true, nullable = false)
	private String name;

	@Column(name = "MOVIES")
	@ManyToMany(fetch = FetchType.LAZY, mappedBy = "producers")
	private Set<Movie> movies;

	Producer() {

	}

	public Producer(String name) {
		this.name = name;
	}

	public Integer getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public Set<Movie> getMovies() {
		return movies;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (obj == null || getClass() != obj.getClass()) {
			return false;
		}

		Producer other = (Producer) obj;
		return name.equals(other.name);
	}

}
