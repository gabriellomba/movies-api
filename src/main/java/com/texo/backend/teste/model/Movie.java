package com.texo.backend.teste.model;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name = "MOVIE")
public class Movie implements Serializable {

	private static final long serialVersionUID = 3004188034464809072L;

	@Id
	@GeneratedValue
	@Column(name = "ID")
	private Integer id;

	@Column(name = "YEAR", nullable = false)
	private Integer year;

	@Column(name = "TITLE", unique = true, nullable = false)
	private String title;

	@Column(name = "STUDIOS")
	@ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
	private Set<Studio> studios = new HashSet<>();

	@Column(name = "PRODUCERS")
	@ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
	private Set<Producer> producers = new HashSet<>();

	@Column(name = "WINNER")
	private Boolean winner;

	Movie() {

	}

	public Movie(Integer year, String title, Boolean winner) {
		this.year = year;
		this.title = title;
		this.winner = winner;
	}

	public void addStudio(Studio studio) {
		studios.add(studio);
	}

	public void addProducer(Producer producer) {
		producers.add(producer);
	}
	
	public Integer getId() {
		return id;
	}

	public Integer getYear() {
		return year;
	}

	public String getTitle() {
		return title;
	}

	public Set<Studio> getStudios() {
		return studios;
	}

	public Set<Producer> getProducers() {
		return producers;
	}

	public Boolean isWinner() {
		return winner;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + title.hashCode();
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (obj == null || getClass() != obj.getClass()) {
			return false;
		}

		Movie other = (Movie) obj;
		return title.equals(other.title);
	}

}
