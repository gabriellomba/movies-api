package com.texo.backend.teste.model;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name = "STUDIO")
public class Studio implements Serializable {

	private static final long serialVersionUID = 6146012905032981565L;

	@Id
	@GeneratedValue
	@Column(name = "ID")
	private Integer id;

	@Column(name = "NAME", unique = true, nullable = false)
	private String name;

	@Column(name = "MOVIES")
	@ManyToMany(fetch = FetchType.LAZY, mappedBy = "studios")
	private Set<Movie> movies;

	Studio() {

	}

	public Studio(String name) {
		this.name = name;
	}

	public Integer getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public Set<Movie> getMovies() {
		return movies;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (obj == null || getClass() != obj.getClass()) {
			return false;
		}

		Studio other = (Studio) obj;
		return name.equals(other.name);
	}

}
