package com.texo.backend.teste.csv;

import java.io.IOException;
import java.util.Set;

import com.texo.backend.teste.model.Movie;

public interface MovieCsvReader {

	Set<Movie> getMovies() throws IOException;

}
