package com.texo.backend.teste.csv;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.texo.backend.teste.error.EmptyCsvException;
import com.texo.backend.teste.model.Movie;
import com.texo.backend.teste.model.Producer;
import com.texo.backend.teste.model.Studio;

@Component
final class MovieCsvReaderImpl implements MovieCsvReader {

	@Value("${movies-file.path}")
	private String filePath;

	@Value("${movies-file.value-separator}")
	private String valueSeparator;

	@Value("${movies-file.year-field}")
	private String yearField;

	@Value("${movies-file.title-field}")
	private String titleField;

	@Value("${movies-file.studios-field}")
	private String studiosField;

	@Value("${movies-file.studios-separator}")
	private String studiosSeparator;

	@Value("${movies-file.producers-field}")
	private String producersField;

	@Value("${movies-file.producers-separator}")
	private String producersSeparator;

	@Value("${movies-file.winner-field}")
	private String winnerField;

	@Value("${movies-file.winner-value}")
	private String winnerValue;

	@Override
	public final Set<Movie> getMovies() throws IOException {
		final Path path = Paths.get(filePath);

		final Map<String, Integer> fieldsIndexes = getFieldsIndexes(path);

		final Set<Movie> movies = new HashSet<>();
		final Map<String, Studio> studiosMap = new HashMap<>();
		final Map<String, Producer> producersMap = new HashMap<>();

		try (Stream<String> fileLines = Files.lines(path)) {
			final Integer yearIdx = fieldsIndexes.get(yearField);
			final Integer titleIdx = fieldsIndexes.get(titleField);
			final Integer studiosIdx = fieldsIndexes.get(studiosField);
			final Integer producersIdx = fieldsIndexes.get(producersField);
			final Integer winnerIdx = fieldsIndexes.get(winnerField);

			fileLines.skip(1).forEach(line -> {
				final String[] mainParts = line.trim().split(valueSeparator);

				final Integer year = Integer.valueOf(mainParts[yearIdx]);
				final String title = mainParts[titleIdx];
				final Boolean isWinner = mainParts.length > winnerIdx && mainParts[winnerIdx].equals(winnerValue);
				final Movie newMovie = new Movie(year, title, isWinner);

				final String[] producersNames = mainParts[producersIdx].split(producersSeparator);
				addProducers(newMovie, producersNames, producersMap);

				final String[] studiosNames = mainParts[studiosIdx].split(studiosSeparator);
				addStudios(newMovie, studiosNames, studiosMap);

				movies.add(newMovie);
			});
		}

		return movies;
	}

	private final Map<String, Integer> getFieldsIndexes(Path path) throws IOException {
		final Map<String, Integer> fieldsIndexes = new HashMap<>();

		try (Stream<String> fileLines = Files.lines(path)) {
			Optional<String> firstLine = fileLines.limit(1).findFirst();

			String line = firstLine.orElseThrow(EmptyCsvException::new);

			final String[] fields = line.trim().split(valueSeparator);

			for (int i = 0; i < fields.length; ++i) {
				fieldsIndexes.put(fields[i], i);
			}
		}

		return fieldsIndexes;
	}

	private void addProducers(Movie movie, String[] producersNames, Map<String, Producer> producersMap) {
		for (String producerName : producersNames) {
			Producer producer;
			if (producersMap.containsKey(producerName)) {
				producer = producersMap.get(producerName);
			} else {
				producer = new Producer(producerName);
				producersMap.put(producerName, producer);
			}

			movie.addProducer(producer);
		}
	}

	private void addStudios(Movie movie, String[] studiosNames, Map<String, Studio> studiosMap) {
		for (String studioName : studiosNames) {
			Studio studio;
			if (studiosMap.containsKey(studioName)) {
				studio = studiosMap.get(studioName);
			} else {
				studio = new Studio(studioName);
				studiosMap.put(studioName, studio);
			}

			movie.addStudio(studio);
		}
	}

}
