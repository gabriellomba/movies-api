package com.texo.backend.teste.listener;

import java.io.IOException;
import java.util.Set;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import com.texo.backend.teste.csv.MovieCsvReader;
import com.texo.backend.teste.dao.MovieDao;
import com.texo.backend.teste.model.Movie;

@Component
public class ApplicationReadyListener {

	@Autowired
	private MovieCsvReader movieCsvReader;

	@Autowired
	private MovieDao movieDao;

	@EventListener(ApplicationReadyEvent.class)
	@Transactional
	public void feedDatabase() throws IOException {
		Set<Movie> movies = movieCsvReader.getMovies();

		movieDao.saveAll(movies);
	}

}
