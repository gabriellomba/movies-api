package com.texo.backend.teste.dto;

import java.util.List;

public final class YearWinCountResponseDto {

	private final List<YearWinCountDto> years;

	public YearWinCountResponseDto(List<YearWinCountDto> years) {
		this.years = years;
	}

	public List<YearWinCountDto> getYears() {
		return years;
	}
}
