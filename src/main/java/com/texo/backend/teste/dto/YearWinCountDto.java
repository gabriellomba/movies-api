package com.texo.backend.teste.dto;

public final class YearWinCountDto {

	private final Integer year;

	private final Long winnerCount;

	public YearWinCountDto(Integer year, Long winnerCount) {
		this.year = year;
		this.winnerCount = winnerCount;
	}

	public Integer getYear() {
		return year;
	}

	public Long getWinnerCount() {
		return winnerCount;
	}
}
