package com.texo.backend.teste.dto;

import java.util.List;

public final class StudioWinCountResponseDto {

	private final List<StudioWinCountDto> studios;

	public StudioWinCountResponseDto(List<StudioWinCountDto> studios) {
		this.studios = studios;
	}

	public List<StudioWinCountDto> getStudios() {
		return studios;
	}
}
