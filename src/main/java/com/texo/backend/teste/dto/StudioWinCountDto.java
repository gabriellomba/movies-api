package com.texo.backend.teste.dto;

public final class StudioWinCountDto {

	private final String name;

	private final Long winCount;

	public StudioWinCountDto(String name, Long winnerCount) {
		this.name = name;
		this.winCount = winnerCount;
	}

	public String getName() {
		return name;
	}

	public Long getWinCount() {
		return winCount;
	}
}
