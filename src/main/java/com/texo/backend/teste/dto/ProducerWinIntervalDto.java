package com.texo.backend.teste.dto;

public final class ProducerWinIntervalDto {

	private final String producer;

	private final Integer interval;

	private final Integer previousWin;

	private final Integer followingWin;

	public ProducerWinIntervalDto(String producer, Integer interval, Integer previousWin, Integer followingWin) {
		this.producer = producer;
		this.interval = interval;
		this.previousWin = previousWin;
		this.followingWin = followingWin;
	}

	public String getProducer() {
		return producer;
	}

	public Integer getInterval() {
		return interval;
	}

	public Integer getPreviousWin() {
		return previousWin;
	}

	public Integer getFollowingWin() {
		return followingWin;
	}
}
