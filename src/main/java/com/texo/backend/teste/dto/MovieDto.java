package com.texo.backend.teste.dto;

import java.util.Set;

public class MovieDto {

	private final Integer id;

	private final Integer year;

	private final String title;

	private final Set<String> studios;

	private final Set<String> producers;

	private final Boolean winner;

	public MovieDto(Integer id, Integer year, String title, Set<String> studios,
			Set<String> producers, Boolean winner) {
		this.id = id;
		this.year = year;
		this.title = title;
		this.studios = studios;
		this.producers = producers;
		this.winner = winner;
	}

	public Integer getId() {
		return id;
	}

	public Integer getYear() {
		return year;
	}

	public String getTitle() {
		return title;
	}

	public Set<String> getStudios() {
		return studios;
	}

	public Set<String> getProducers() {
		return producers;
	}

	public Boolean isWinner() {
		return winner;
	}

}
