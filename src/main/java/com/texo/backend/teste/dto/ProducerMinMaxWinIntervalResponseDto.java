package com.texo.backend.teste.dto;

public final class ProducerMinMaxWinIntervalResponseDto {

	private final ProducerWinIntervalDto min;

	private final ProducerWinIntervalDto max;

	public ProducerMinMaxWinIntervalResponseDto(ProducerWinIntervalDto min, ProducerWinIntervalDto max) {
		this.min = min;
		this.max = max;
	}

	public ProducerWinIntervalDto getMin() {
		return min;
	}

	public ProducerWinIntervalDto getMax() {
		return max;
	}
}
