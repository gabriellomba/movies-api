package com.texo.backend.teste.service;

import java.util.Set;

import com.texo.backend.teste.dto.MovieDto;
import com.texo.backend.teste.dto.YearWinCountResponseDto;

public interface MovieService {

	public Set<MovieDto> getWinners(Integer year);

	public void delete(Integer id);

	public YearWinCountResponseDto getYearWinCount(Long biggerThan);
}
