package com.texo.backend.teste.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import com.texo.backend.teste.dao.ProducerDao;
import com.texo.backend.teste.dto.ProducerMinMaxWinIntervalResponseDto;
import com.texo.backend.teste.dto.ProducerWinIntervalDto;
import com.texo.backend.teste.error.ProducerWithMoreThanOneWinNotFoundException;

@Service
public class ProducerServiceImpl implements ProducerService {

	@Autowired
	private ProducerDao producerDao;

	@Override
	public ProducerMinMaxWinIntervalResponseDto getProducersMinMaxWinInterval() {
		List<ProducerWinIntervalDto> producerWithMinInterval =
				producerDao.getProducerMinWinInterval(PageRequest.of(0, 1));

		if (producerWithMinInterval.isEmpty()) {
			throw new ProducerWithMoreThanOneWinNotFoundException();
		} else {
			List<ProducerWinIntervalDto> producerWithMaxInterval =
					producerDao.getProducerMaxWinInterval(PageRequest.of(0, 1));

			return new ProducerMinMaxWinIntervalResponseDto(
					producerWithMinInterval.get(0), producerWithMaxInterval.get(0));
		}
	}

}
