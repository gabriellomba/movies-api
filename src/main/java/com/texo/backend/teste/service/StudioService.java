package com.texo.backend.teste.service;

import com.texo.backend.teste.dto.StudioWinCountResponseDto;

public interface StudioService {

	public StudioWinCountResponseDto getStudiosWinCount();
}
