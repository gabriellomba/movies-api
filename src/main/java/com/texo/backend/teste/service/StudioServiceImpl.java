package com.texo.backend.teste.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.texo.backend.teste.dao.StudioDao;
import com.texo.backend.teste.dto.StudioWinCountResponseDto;

@Service
public class StudioServiceImpl implements StudioService {

	@Autowired
	private StudioDao studioDao;

	@Override
	public StudioWinCountResponseDto getStudiosWinCount() {
		return new StudioWinCountResponseDto(studioDao.getStudiosWinCount());
	}

}
