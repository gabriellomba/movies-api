package com.texo.backend.teste.service;

import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.texo.backend.teste.dao.MovieDao;
import com.texo.backend.teste.dto.MovieDto;
import com.texo.backend.teste.dto.YearWinCountResponseDto;
import com.texo.backend.teste.error.MovieNotFoundException;
import com.texo.backend.teste.error.ValidationException;
import com.texo.backend.teste.model.Movie;
import com.texo.backend.teste.model.Producer;
import com.texo.backend.teste.model.Studio;

@Service
public class MovieServiceImpl implements MovieService {

	@Autowired
	private MovieDao movieDao;

	@Override
	public Set<MovieDto> getWinners(Integer year) {
		return movieDao.getWinnersInYear(year).stream()
				.map(this::transformMovieToDto)
				.collect(Collectors.toSet());
	}

	private MovieDto transformMovieToDto(Movie movie) {
		Set<String> producersNames = movie.getProducers().stream()
				.map(Producer::getName)
				.collect(Collectors.toSet());
		
		Set<String> studiosNames = movie.getStudios().stream()
				.map(Studio::getName)
				.collect(Collectors.toSet());
		return new MovieDto(movie.getId(), movie.getYear(), movie.getTitle(),
				producersNames, studiosNames, movie.isWinner());
	}

	@Override
	public void delete(Integer id) {
		Movie movie = movieDao.findById(id).orElseThrow(() -> new MovieNotFoundException(id));

		if (movie.isWinner()) {
			throw new ValidationException(HttpStatus.BAD_REQUEST, "Não é possível excluir filmes vencedores.");
		} else {
			movieDao.delete(movie);
		}
	}

	@Override
	public YearWinCountResponseDto getYearWinCount(Long biggerThan) {
		return new YearWinCountResponseDto(movieDao.getYearWinCount(biggerThan));
	}

}
