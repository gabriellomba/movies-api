package com.texo.backend.teste.service;

import com.texo.backend.teste.dto.ProducerMinMaxWinIntervalResponseDto;

public interface ProducerService {

	public ProducerMinMaxWinIntervalResponseDto getProducersMinMaxWinInterval();
}
