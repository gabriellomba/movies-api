# Movies API

Este projeto provê uma API RESTful que lê e mantém a lista de indicados e vencedores da categoria **Pior Filme** do *Golden Raspberry Awards*.

## Pré requisitos

Para executar o projeto, é necessário ter instalado:

 - Java 8
 - Maven (ou uma IDE que já venha com o plugin do Maven, como o Eclipse)

## Execução

Para executar o projeto, primeiramente, é necessário criar o artefato gerado pelo Maven, através do Eclipse ou através do comando (estando no diretório principal do projeto):

`mvn package`

Após gerar o artefato, este se encontrará na pasta *target*, gerada pelo Maven, com o nome *teste-0.0.1.jar*. Para subir a aplicação, basta executar o seguinte comando:

`java -jar target/teste-0.0.1.jar`

A partir deste comando, caso seja executada localmente, a aplicação já estará funcionando [aqui](http://localhost:8080) e o console do banco H2 será mostrado [aqui](http://localhost:8080/h2-console).
Para utilizar o console para inspecionar os dados nas tabelas, tenha certeza de que esteja usando **jdbc:h2:mem:testdb** como a URL JDBC, no momento do login.

## Funcionalidades

Os funcionalidades da API podem ser verificadas através dos seguintes *urls* relativas:

 1. **Obter o(s) vencedor(es), informando um ano:** Requisição *GET* no *endpoint* **/movies/winners?year={anoDesejado}**.
 2. **Obter os anos que tiveram mais de um vencedor:** Requisição *GET* no *endpoint* **/movies/yearWinCount?biggerThan={quantidade}**. Este ítem é atendido ao passar o valor da quantidade igual a 1.
 3. **Obter a lista de estúdios, ordenada pelo número de premiações:** Requisição *GET* no *endpoint* **/studios/winCount**. Foi assumido que a ordenação dos estúdios seria decrescente na quantidade de títulos.
 4. **Obter o produtor com maior intervalo entre dois prêmios, e o que obteve dois prêmios mais rápido:** Requisição *GET* no *endpoint* **/producers/minMaxWinInterval**. Quando não há produtores com mais que um título (o que impossibilita o retorno dos dados), a API retorna uma mensagem de erro adequada, com o *status* HTTP 404.
 5. **Excluir um filme. Não deve permitir excluir vencedores:** Requisição *DELETE* no *endpoint* **/movies/{id}**. Quando não existe um filme com o id especificado, a API retorna um erro com *status* 404 e quando um filme é encontrado mas é vencedor, a API retorna erro com *status* 400. Em ambas as exceções, dados da requisição e mensagens apropriadas são retornadas.

Além disso, a API foi desenvolvida de maneira a depender dos dados no arquivo *application.properties* do framework Spring, que possibilita externalizar a configuração, como é discutido [aqui](https://docs.spring.io/spring-boot/docs/current/reference/html/boot-features-external-config.html).

## Testes

Para executar os testes, também se faz uso do Maven, executando o comando:

`mvn test`

Após o comando, os testes executarão subindo o contexto do Spring Boot conforme necessário.

## Tecnologias

* [Java 8](https://www.oracle.com/technetwork/java/javase/overview/java8-2100321.html) - Linguagem usada
* [Maven](https://maven.apache.org/) - Gerenciamento de dependências e *builds* e execução de testes
* [Spring Boot](https://spring.io/projects/spring-boot) - *Framework* utilizado
* [H2 Database](http://www.h2database.com/html/main.html) - Banco de dados em memória utilizado
* [Spring Data](http://projects.spring.io/spring-data/) - Utilitário para manusear objetos vindos do banco
* [JUnit](https://junit.org/junit5) - *Framework* de teste utilizado
